<?php
require_once "Conexion.php";
require_once "Metodos.php";
session_start();
error_reporting(0);
$sql = "SELECT * FROM carousel";
$obj = new methods();
$data=$obj->view_data($sql); 

$obj1 = new methods();
$sql1="SELECT * FROM datos_generales";
$DGenerales=$obj1->buscar($sql1);

$sql2 = "SELECT * FROM social";
$obj2 = new methods();
$social=$obj2->view_data($sql2); 

$nom_usuario =$_SESSION['nom_usuario'];
$sql3 = "SELECT * FROM proforma WHERE nom_usuario='$nom_usuario'";
$obj3 = new methods();
$proforma=$obj3->view_data($sql3); 

$sql4 = "SELECT * FROM producto";
$obj4 = new methods();
$producto=$obj4->view_data($sql4); 

if ($_SESSION['tipo_persona']=="administrador") {
  header("Location:menu_administrador.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 



	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
  <script type="text/javascript" src="engine1/jquery.js"></script>
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50" background="<?php echo $DGenerales['fondo'];?>" style="font-family: 'Open Sans Condensed', sans-serif;
font-family: 'Indie Flower', cursive;" >


<div class="site-wrapper">

<header>



    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
  
  
    <a href="index.php"class="fa fa-toggle-left padleft" style="font-size:48px;color:white;"></a>    

           <a class="navbar-brand" href="#">PROFORMA</a

    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav mr-auto"></div>
      <div class="navbar-nav">          
      <ul class="nav navbar-nav " id="pills-tab" role="tablist">
     
        
      </ul>
      </div>
    </div>
  </nav>

</header>
</div>

<div class="pad"></div>
<div class="pad"></div>



<center><h3>Mis Proformas</h3>
<div class="container">
<div class="pad"></div>

    <form action="proformasesion.php"method="post">
    <div class="form-group col-6">
           <select class="form-control" name="list_proforma">
         
           <?php              


    
          
               foreach($proforma as $ke)
                   {       
               ?>   
                     <option><?php echo $ke['nombre'];?></option>
               
                      
               <?php                
                 }    
    
               ?>
      

            </select>
            </div>
           
          
            
              <div class="form-group">
                <input type="submit"class="btn btn-secondary"value="Aceptar"> 
                </div>
            

  

        
        
    </form>
    <a class="nav-link" href=""data-toggle="modal" data-target="#crearproforma">Crear Proforma</a>
    </center>


</div>


    











<!-- Modal crearproforma-->
<div class="modal fade" id="crearproforma" tabindex="-1" role="dialog" aria-labelledby="crearproformaTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="crearproformaTitle">Crear Proforma</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="Operacion_Insertar_PRO.php" method="post">
  

                <div  class="form-group">
                <label>Nombre</label>
                <input type="text"name="txt_nombrepro"class="form-control"placeholder="Nombre...">  
                </div>
                <input type="text"name="txt_nomusuario"class="txtv"value="<?php echo $_SESSION['nom_usuario'] ;?>">  
              <input class="btn btn-secondary btn-block" type="submit" value="Guardar">  
        </form>
      </div>

      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


</body>
</html>