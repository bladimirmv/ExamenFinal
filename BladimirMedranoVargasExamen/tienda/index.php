<?php
require_once "Conexion.php";
require_once "Metodos.php";
session_start();
error_reporting(0);
$sql = "SELECT * FROM carousel";
$obj = new methods();
$data=$obj->view_data($sql); 

$obj1 = new methods();
$sql1="SELECT * FROM datos_generales";
$DGenerales=$obj1->buscar($sql1);

$sql2 = "SELECT * FROM social";
$obj2 = new methods();
$social=$obj2->view_data($sql2); 

$sql3 = "SELECT * FROM categoria";
$obj3 = new methods();
$Categoria=$obj3->view_data($sql3); 

$sql4 = "SELECT * FROM producto";
$obj4 = new methods();
$producto=$obj4->view_data($sql4); 

if ($_SESSION['tipo_persona']=="administrador") {
  header("Location:menu_administrador.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 




	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
  <script type="text/javascript" src="engine1/jquery.js"></script>
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">


    

    <title>Document</title>
</head>
<script>
  $('.carousel').carousel({
  interval: 1000
})
</script>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50" background="<?php echo $DGenerales['fondo'];?>" style="font-family: 'Open Sans Condensed', sans-serif;
font-family: 'Indie Flower', cursive;" >
<div class="site-wrapper">

  <header>



      <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
                         <?php
                        
                        if ($_SESSION['tipo_persona']=="cliente")

                        {
                          
                          echo '' ;
    
                          echo '<a href="menu_proforma.php" class=" fa fa-calendar-o padleft" style="font-size:48px;"></a>' ;
               
                      
                        }                        
                        else {
                          echo '<a href="carrito2.php" class=" fa fa-calendar-o padleft" style="font-size:48px;"class="nav-link"href="#"data-toggle="modal" data-target="#iniciarsesion"></a>';
               
                          

                         
                        }
                        ?>



             <a class="navbar-brand" href="#"><?php echo $_SESSION['nombre'] ;?></a>
           
      

      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav mr-auto"></div>
        <div class="navbar-nav">          
        <ul class="nav navbar-nav " id="pills-tab" role="tablist">
        <li class="nav-item"> <a class="nav-item nav-link" class="nav-link" id="pills-inicio-tab" data-toggle="pill" href="#pills-inicio" role="tab" aria-controls="pills-inicio" aria-selected="true">Inicio <span class="sr-only">(current)</span></a></li>

  
                         <?php
                        
                        if ($_SESSION['tipo_persona']=="cliente")
                        {
    
                        //  echo '<li class="nav-item"><a class="nav-link" id="pills-micuenta-tab" data-toggle="pill" href="#pills-micuenta" role="tab" aria-controls="pills-micuenta" aria-selected="true">    Mi Cuenta<span class="sr-only">(current)</span></a> </li>' ;
                          echo '<li><a class="nav-link" href="Desconectar.php"> Cerrar Sesión</a></li>';
                      
                        }                        
                        else {
                          echo '<li  class="nav-item"><a class="nav-link" href="#"data-toggle="modal" data-target="#crearcuenta">Crear Cuenta</a></li>' ;                            
                          
                          echo '<li><a class="nav-link"href="#"data-toggle="modal" data-target="#iniciarsesion"> Iniciar Sesión</a></li>' ;
                         
                        }
                        ?>
          
        </ul>
        </div>
      </div>
    </nav>

  </header>



</div>



  <div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-inicio" role="tabpanel" aria-labelledby="pills-inicio-tab">
    
<div class="pad"></div>
  <div class="jumbotron jumbotron-fluid ">
  <div class="container">
    <center>
    <img class="mb-2 rounded-circle " src="<?php echo $DGenerales['icono'];?>" alt="" width="200" height="200">
    <h1 class="display-4"><?php echo $DGenerales['nombre'];?></h1>
    <p class="lead"><?php echo $DGenerales['subnombre'];?></p>
        </center>
  </div>
</div>


<div id="wowslider-container1">
<div class="ws_images"><ul>
            <?php
             
             foreach($data as $key)
                 {
                  ?>
                  <li><img src="<?php echo $key['imagen'] ?>" alt="<?php echo $key['nombre'] ?>" title="<?php echo $key['nombre'] ?>"/></li>

                  <?php     
                  
             
                }
     
             ?>

</ul></div>

</div>	








<div class="pad3"></div>


  <div class="jumbotron jumbotron-fluid">
  <div class="container">

    <center>
    <h1 class="display-4">Redes Sociales</h1>
    <p class="lead">Visita nuestras redes sociales</p>
               <?php              
               foreach($social as $key1)
                   {       
               ?>    

               <a href="<?php echo $key1['link'];?>"><i class="fa fa-<?php echo $key1['nombre'];?>" style="font-size:48px;color:white;padding-left:10px;"></i></a> 
       
               <?php                
                 }       
               ?>
               <div>Contactanos al 76969445</div>
        </center>
  </div>
</div>


</div>



  <div class="tab-pane fade" id="pills-micuenta" role="tabpanel" aria-labelledby="pills-micuenta-tab">
  <div class="container">
            <div class="row profile">
            <div class="col-md-3">
              <div class="profile-sidebar">

                <hr>
                <div class="profile-userpic">
                  <img src="img/default-user-profile.svg" class="img-responsive" alt="">
                </div>

                <div class="profile-usertitle">
                  <div class="profile-usertitle-name">
                  <strong><?php echo $_SESSION['nombre'].' '. $_SESSION['apellidos'];?></strong>
                  </div>
                  <div class="profile-usertitle-job">
            
                CI:<?php echo $_SESSION['ci']?>
                Celular: <?php echo $_SESSION['celular']?>
                  <?php echo $_SESSION['correo']?>
                  </div>
                </div>

                <div class="profile-userbuttons">

                </div>

              </div>
            </div>
            <div class="col-md-9">
                    <div class="profile-content">
                      <hr>
                      Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolores est facilis voluptatem quis aut enim ipsa cum! Libero soluta aut at deserunt? Culpa maiores maxime adipisci necessitatibus nostrum modi facilis.
                      Sequi, nesciunt unde veniam sed, veritatis fuga molestiae nostrum magni, consequatur tempore iure ex sapiente eum laborum molestias quia aliquam quaerat? Vel aspernatur, itaque odio sint maiores dignissimos quidem dolorum.
                      Id aliquid minima temporibus, optio alias reprehenderit repellendus magnam fugit expedita. Labore, itaque, iusto rerum laboriosam earum fugit asperiores quasi voluptate consequuntur distinctio facilis explicabo cum, voluptatibus accusantium commodi minima!
                      Vel, quia ut rem eveniet, sed incidunt blanditiis officiis aliquid tempore consequuntur vitae ab voluptatibus! Cumque ratione saepe enim amet perferendis ut facere, necessitatibus maiores, sapiente labore rem veniam earum.
                      Consequatur placeat id ipsa nostrum adipisci eos assumenda quos, mollitia eligendi deserunt beatae voluptas reprehenderit magni perspiciatis, voluptatem, ex officiis officia nemo exercitationem laborum nam iusto. Amet dolorem animi minima?
                      Aliquid explicabo quos illo at, asperiores laudantium ex perferendis quod dolore ea, optio ullam obcaecati laboriosam non nobis velit culpa numquam sint, quam excepturi autem molestiae ratione. Cum, quidem fuga.
                      Vero et, sint illum recusandae ex pariatur libero, voluptatibus quia sed modi nisi vel. Quos exercitationem ab ipsa, vero cum fugiat beatae cumque inventore velit voluptatibus expedita magnam totam porro.
                      Ipsam voluptatum quod quia illum temporibus labore. Quas, doloremque voluptas nemo numquam accusantium doloribus odit ratione ullam tempora sint porro voluptatem architecto neque vitae error sapiente maxime unde omnis asperiores.
                      Voluptates cupiditate quaerat facilis quas itaque facere atque nemo ex earum amet explicabo accusamus deleniti rerum eius dignissimos harum pariatur voluptatum ipsum sunt, esse commodi officia rem omnis? Illo, laudantium.
                      Esse, quod. Doloribus, veniam. Aliquid ex numquam tenetur ea nesciunt dolores odit quam rerum. Perferendis omnis consequatur eos, sint, culpa repudiandae mollitia quae modi ut nihil maiores dicta nesciunt quidem!
                      Ea commodi maxime aut iste totam? Natus, adipisci delectus pariatur, deleniti reprehenderit sunt ex nisi optio aperiam, dolores placeat. Laudantium laborum quaerat at id perspiciatis. Itaque perferendis facilis natus similique.
                      Assumenda voluptatum cum blanditiis maiores laborum, dolore eius, aperiam ad aliquam atque expedita enim rerum sit commodi eveniet rem sapiente quidem iure ea, ipsum itaque! Error eos repellendus pariatur quod.
                      Suscipit et ut soluta ea. Aliquam corporis molestias recusandae, veritatis, rerum neque porro dolorum praesentium aperiam iusto tempora repudiandae reprehenderit est sed, excepturi ratione! Quam eveniet iste asperiores vel aut?
                      Perspiciatis pariatur minima alias? Iusto, facere! Animi autem dolores asperiores laboriosam ea eveniet quas labore quo. Minus recusandae deserunt, unde assumenda veritatis amet ab veniam expedita possimus nemo similique dolorem.
                      Vero labore quo architecto hic corporis cum sapiente consequuntur reiciendis facilis beatae dolore voluptatum iure libero blanditiis, omnis quos adipisci placeat dolorum modi velit iusto tempora ullam excepturi itaque. Perspiciatis!
                      Vel repudiandae quas vero qui, mollitia voluptates aperiam accusantium quis, expedita exercitationem iure fugit ab reprehenderit, nostrum culpa tempore ea! Modi veritatis qui cumque fugiat consequatur corrupti et fugit asperiores.
                      Quibusdam ipsam itaque possimus fugiat reprehenderit, qui optio! Perspiciatis fugiat repellendus harum officia asperiores tempora commodi. Recusandae non excepturi quis rerum ut ad rem, porro magnam neque eveniet alias officiis.
                      Repudiandae cumque modi ab inventore tempora, dolore eius illo? Et, necessitatibus. Amet necessitatibus quisquam magnam, ex, eligendi adipisci et nulla sed asperiores fuga molestias quos quam sunt laborum impedit. Excepturi!
                      Atque expedita veniam beatae excepturi. Autem labore ex nulla repudiandae! Ut aliquid dolorem nemo iure. Ab accusamus, illo aspernatur error suscipit consequuntur distinctio similique voluptatem, tempora quia nihil est eos!
                      In adipisci quos rem fugit ad sit velit laborum sed excepturi enim illum cum accusantium dolores eaque quasi, perspiciatis, ut dolorem aspernatur provident autem deserunt unde officia. Suscipit, aliquid sapiente.
                  </div>
            </div>
          </div>
        </div>
</div>











<!-- Modal iniciarsesion-->

<div class="modal fade" id="iniciarsesion" tabindex="-1" role="dialog" aria-labelledby="iniciarsesionTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="iniciarsesionTitle">Iniciar Sesión</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

       <form action="Operacion_validacion.php" method="post">
       <div class="form-group">
                <label>Correo electronico</label>
                <input type="text"name="txt_correo"class="form-control"placeholder="Correo Electronico...">  
                </div>

                <div class="form-group">
                <label>Contraseña</label>
                <input type="password"name="txt_contrasenha"class="form-control"placeholder="Contraseña..."> 
                </div>
          <input class="btn btn-secondary btn-block" type="submit" value="Iniciar"> 
        </form>  
      <a class="nav-link" href=""data-toggle="modal" data-target="#crearcuenta">Crear Cuenta</a>

      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


<!-- Modal crearcuenta-->
<div class="modal fade" id="crearcuenta" tabindex="-1" role="dialog" aria-labelledby="crearcuentaTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="crearcuentaTitle">Crear Cuenta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="Operacion_Insertar_CL.php" method="post">
        <div class="form-group">
                <label>Usuario</label>
                <input type="text"name="txt_correo"class="form-control"placeholder="Usuario...">  
                </div>

                <div class="form-group">
                <label>Contraseña</label>
                <input type="password"name="txt_contrasenha"class="form-control"placeholder="Contraseña...">  
              </div>               

                <div  class="form-group">
                <label>Nombre</label>
                <input type="text"name="txt_nombre"class="form-control"placeholder="Nombre...">  
                </div>
                <div class="form-group">
                <label>Apellidos</label>
                <input type="text"name="txt_apellidos"class="form-control"placeholder="Apellidos...">  
                </div>


               
              <input class="btn btn-secondary btn-block" type="submit" value="Guardar">  
        </form>
      </div>

      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


	<script type="text/javascript" src="engine1/wowslider.js"></script>
	<script type="text/javascript" src="engine1/script.js"></script>
</body>
</html>