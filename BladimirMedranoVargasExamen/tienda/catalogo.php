<?php
require_once "Conexion.php";
require_once "Metodos.php";
session_start();
error_reporting(0);
$nom_proforma= $_SESSION['proforma'];
$obj1 = new methods();
$sql1="SELECT * FROM datos_generales";
$DGenerales=$obj1->buscar($sql1);

$sql3 = "SELECT * FROM categoria";
$obj3 = new methods();
$Categoria=$obj3->view_data($sql3); 

$sql4 = "SELECT * FROM producto";
$obj4 = new methods();
$producto=$obj4->view_data($sql4); 

if ($_SESSION['tipo_persona']=="administrador") {
  header("Location:menu_administrador.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 


<script src="js/script.js"></script>

	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
  <script type="text/javascript" src="engine1/jquery.js"></script>
  
  <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <title>Document</title>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50" background="<?php echo $DGenerales['fondo'];?>" style="font-family: 'Open Sans Condensed', sans-serif;
font-family: 'Indie Flower', cursive;" >
<div class="site-wrapper">

<header>



    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
  
  
    <a href="listaproforma.php"class="fa fa-toggle-left padleft" style="font-size:48px;color:white;"></a>    

           <a class="navbar-brand" href="#">Catalogo</a

    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav mr-auto"></div>
      <div class="navbar-nav">          
      <ul class="nav navbar-nav " id="pills-tab" role="tablist">
     
        
      </ul>
      </div>
    </div>
  </nav>

</header>
</div>
<div class="pad"></div>
<div class="pad"></div>





		<div class="store-wrapper">
			 <div class="category_list">
         
				<a  class="category_item btn btn-secondary" category="all">Todo</a>
       <?php              
               foreach($Categoria as $key2)
                   {       
               ?>   
               
			       	<a class="category_item btn btn-secondary" category="<?php echo $key2['nombre'];?>"><?php echo $key2['nombre'];?></a> 
                      
               <?php                
                 }       
               ?>




			</div>
			<section class="products-list">


               <?php              
               foreach($producto as $key3)
                   {       
               ?>   
                     <div class="product-item" category="<?php echo $key3['categoria'];?>">
                     

                    <img src="<?php echo $key3['imgen'];?>" alt=""  >        
                    <span><?php echo $key3['nombre'];?></span>
                    <a href="detalles.php?id=<?php echo $key3['numero'];?>">Ver</a>
                   </div>               
			       
                      
               <?php                
                 }       
               ?>


			</section>
		</div>
	</div>







 




    
</body>
</html>