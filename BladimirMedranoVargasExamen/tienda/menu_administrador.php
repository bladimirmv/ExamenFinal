<?php
require_once "Conexion.php";
require_once "Metodos.php";

$sql3 = "SELECT * FROM categoria";
$obj3 = new methods();
$Categoria=$obj3->view_data($sql3); 

$sql4 = "SELECT * FROM producto";
$obj4 = new methods();
$producto=$obj4->view_data($sql4); 

session_start();




if ($_SESSION['tipo_persona']=="cliente"|| $_SESSION['tipo_persona']=="") {
    header("Location:index.php");
  }


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/jquery.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <script src="js/jquery-3.3.1.slim.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
    <script src="js/script.js"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="50">
<div class="site-wrapper">

  <header>


<!-- nav bar-->
      <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
      <img class="rounded-circle" src="img/admin.png" alt="Generic placeholder image" width="80" height="80 ">
      <a class="navbar-brand" href="#"><strong><?php echo $_SESSION['nombre']?></strong></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav mr-auto"></div>
        <div class="navbar-nav">          
        <ul class="nav navbar-nav " id="pills-tab" role="tablist">
        <li class="nav-item"> <a class="nav-item nav-link" class="nav-link" id="pills-inicio-tab" data-toggle="pill" href="#pills-inicio" role="tab" aria-controls="pills-inicio" aria-selected="true">Inicio <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"> <a class="nav-item nav-link" class="nav-link" id="pills-agregarcategoria-tab" data-toggle="pill" href="#pills-agregarcategoria" role="tab" aria-controls="pills-agregarcategoria" aria-selected="true">Categoria <span class="sr-only">(current)</span></a></li>
        <li class="nav-item"> <a class="nav-item nav-link" class="nav-link" id="pills-producto-tab" data-toggle="pill" href="#pills-producto" role="tab" aria-controls="pills-producto" aria-selected="true">Producto <span class="sr-only">(current)</span></a></li>
        <li  class="nav-item"><a class="nav-link" href="#"data-toggle="modal" data-target="#crearcuenta">Crear Cuenta</a></li>
        <li><a class="nav-link" href="Desconectar.php"> Cerrar Sesión</a></li>
        </div>
      </div>
    </nav>
  </header>
  <div class="pad1"></div>
</div>












          

<div class="tab-content" id="pills-tabContent">

    <div class="tab-pane fade show active" id="pills-inicio" role="tabpanel" aria-labelledby="pills-inicio-tab">
        <hr>



        
<!-- Datos Generales-->
<p>
<center> 
<h1 class="jumbotron-heading">Datos Generales</h1>
  <a class="btn btn-secondary" data-toggle="collapse" href="#datosgenerales" role="button" aria-expanded="false" aria-controls="datosgenerales">
  Añadir
</a>
</center> 
</p>
<div class="collapse" id="datosgenerales">
  <div class="card card-body">
  <center> 
      <form action="Operacion_Guardar_DGNRLS.php" method="post"enctype="multipart/form-data">
          <div class="container">
           
                <div class="form-group col-8">
                <label>Nombre</label>
                <input type="text"name="txt_nombre"class="form-control"placeholder="Nombre...">  
                </div>

                <div class="form-group col-8">
                <label>Subnombre</label>
                <input type="text"name="txt_subnombre"class="form-control"placeholder="Subnombre...">  
                </div>

                <div class="form-group col-8">
                <label>Abreviatura</label>
                <input type="text"name="txt_abreviatura"class="form-control"placeholder="Abreviatura...">  
                </div>

                <div class="form-group ">
                <label>Icono(200x200)</label>
                <input type="file"name="txt_ico">  
                </div>
                <div class="form-group ">
                <label>Background (   fondo    )</label>
                <input type="file"name="txt_fondo">  
                </div>

                <div class="form-group ">
                <input type="submit"class="btn btn-secondary"value="Guardar">
                </div>              
         </div>
      </form>
    </center>
    
  </div>
</div>















<!--Redes Sociales-->



<p>
<center> 
<h1 class="jumbotron-heading">Redes Sociales</h1>
  <a class="btn btn-secondary" data-toggle="collapse" href="#redessociales" role="button" aria-expanded="false" aria-controls="redessociales">
    Añadir
  </a>
</center> 
</p>
<div class="collapse" id="redessociales">
  <div class="card card-body">
  <center> 
      <form action="Operacion_Guardar_SOCIAL.php" method="post">
          <div class="container">
                <div class="form-group col-8">
                <label>Red Social</label>
                <input type="text"name="txt_numero"class="form-control"placeholder="Numero">  
                <input type="text"name="txt_nombre"class="form-control"placeholder="Nombre...">  
                <input type="text"name="txt_link"class="form-control"placeholder="Link...">  
                </div>
                <div class="form-group ">
                <input type="submit"class="btn btn-secondary"value="Guardar">
                </div>              
         </div>
      </form>
    </center>
    
  </div>
</div>












<!--  Carrousel-->

<p>
<center> 
<h1 class="jumbotron-heading">Carrousel</h1>
  <a class="btn btn-secondary" data-toggle="collapse" href="#addcarousel" role="button" aria-expanded="false" aria-controls="addcarousel">
    Añadir
  </a>
</center> 
</p>
<div class="collapse" id="addcarousel">
  <div class="card card-body">
  <center> 
      <form action="Operacion_Guardar_CSEL.php" method="post"enctype="multipart/form-data">
          <div class="container">
          <div class="form-group col-8">
                <select class="form-control" name="txt_numero">
                  <option>1</option>
                  <option>2</option>
                  <option>3</option>
                  <option>4</option>
                  <option>5</option>
                  <option>6</option>
                  <option>7</option>
                  <option>8</option>
                  <option>9</option>
                  <option>10</option>
                </select>
                </div>


                  <div class="form-group col-8">
                <label>Nombre</label>
                <input type="text"name="txt_nombre"class="form-control"placeholder="Nombre...">  
                </div>

                <div class="form-group ">
                <input type="file"name="txt_imagen">  
                </div>
  
                <div class="form-group col-8">
                <input type="submit"class="btn btn-secondary"value="Guardar">
                </div>              
         </div>
      </form>
    </center>
    
  </div>
</div>







   
</div>
    <div class="tab-pane fade" id="pills-agregarcategoria" role="tabpanel" aria-labelledby="pills-agregarcategoria-tab">



      <form action="Operacion_Guardar_CAT.php" method="post">
          <div class="container">
          <br><br>
           <h1>Añadir Categoria</h1>
           <br><br>

                <div class="form-group col-6">
                <label>Categoria Numero</label>
                <input type="text"name="txt_numerocat"class="form-control"placeholder="Numero...">  
                </div>
                <div class="form-group col-6">
                <label>Nombre de la Categoria</label>
                <input type="text"name="txt_nombrecat"class="form-control"placeholder="Nombre Categoria...">  
                </div>
                <div class="form-group   col-6">
                <input type="submit"class="btn btn-secondary"value="Guardar">
                </div>              
         </div>
      </form>


       <form action="Operacion_Eliminar_CAT.php" method="post">
          <div class="container">
          <br><br>
           <h1>Eliminar Categoria</h1>
           <br><br>
                <div class="form-group col-6">
                <label>Nombre de la Categoria</label>
                <input type="text"name="txt_nombrecate"class="form-control"placeholder="Nombre Categoria...">  
                </div>
                <div class="form-group   col-6">
                <input type="submit"class="btn btn-secondary"value="Eliminar">
                </div>              
         </div>
      </form>

  

    </div>




    <div class="tab-pane fade" id="pills-producto" role="tabpanel" aria-labelledby="pills-producto-tab">
    <form action="Operacion_Guardar_PROD.php" method="post"enctype="multipart/form-data">
          <div class="container">
          <br><br>
           <h1>Añadir Producto</h1>
           <br><br>
           
           <div class="form-group col-6">
           <select class="form-control" name="list_cat">
         
           <?php              
           $sql = "SELECT * FROM categoria";
           $obj = new methods();
           $cat=$obj->view_data($sql); 
               foreach($cat as $ke)
                   {       
               ?>   
                     <option><?php echo $ke['nombre'];?></option>
               
                      
               <?php                
                 }       
               ?>
      

            </select>
            </div>


                                            <?php
                                            $obj = new methods();
                                            $sql="SELECT count(numero) FROM producto";
                                            $data=$obj->cod_auto($sql); 
                                            foreach($data as $key){ 
                                            ?> 
                                                           <div class="form-group col-6">
                                                            <label>Numero</label>
                                                            <input type="text"name="txt_numeroprod"class="form-control"placeholder="Numero..."value="<?php echo $key+1;?>">  
                                                            </div>                      

                                            <?php   
                                            }
                                            ?>       





                <div class="form-group col-6">
                <label>Nombre</label>
                <input type="text"name="txt_nombreprod"class="form-control"placeholder="Nombre...">  
                </div> 
                <div class="form-group col-6">
                <label>Precio</label>
                <input type="text"name="txt_precioprod"class="form-control"placeholder="Precio...">  
                </div>
             
               
                <div class="form-group ">
                <input type="file"name="txt_imagen">  
                </div>
                <div class="form-group col-6">
                <label>300x275</label>
                <input type="submit"class="btn btn-secondary"value="Guardar">
                </div>     
                
                
         </div>
      </form>





       <form action="Operacion_Eliminar_PROD.php" method="post">
          <div class="container">
          <br><br>
           <h1>Eliminar Producto</h1>
           <br><br>
                <div class="form-group col-6">
                <label>Nombre de la Producto</label>
                <input type="text"name="txt_nombreprod"class="form-control"placeholder="Nombre Producto...">  
                </div>
                <div class="form-group   col-6">
                <input type="submit"class="btn btn-secondary"value="Eliminar">
                </div>              
         </div>
      </form>



      <div class="wrap">

<div class="store-wrapper">
   <div class="category_list">
     
    <a  class="category_item btn btn-secondary" category="all">Todo</a>
   <?php              
           foreach($Categoria as $key2)
               {       
           ?>   
               <span class=""><?php echo $key2['id'];?></span>
           <a class="category_item btn btn-secondary" category="<?php echo $key2['nombre'];?>"><?php echo $key2['nombre'];?></a> 
       
                  
           <?php                
             }       
           ?>




  </div>
  <section class="products-list">


           <?php              
           foreach($producto as $key3)
               {       
           ?>   
                 <div class="product-item" category="<?php echo $key3['categoria'];?>">
                <span class="badge badge-primary badge-pill"><?php echo $key3['numero'];?></span>
                <img src="<?php echo $key3['imgen'];?>" alt="" >        
                <a href="#"><?php echo $key3['nombre'];?></a>
               </div>
           
         
                  
           <?php                
             }       
           ?>


  </section>
</div>
</div>


</div>
</div>



    </div>





    
</div>
<!-- Modal crearcuenta-->
<div class="modal fade" id="crearcuenta" tabindex="-1" role="dialog" aria-labelledby="crearcuentaTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="crearcuentaTitle">Crear Cuenta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="Operacion_Insertar_ADM.php" method="post">
                <div class="form-group">
                <label>Cedula de Identidad</label>
                <input type="text"name="txt_ci"class="form-control"placeholder="C.I. solo numeros...">  
                </div>
                <div  class="form-group">
                <label>Nombre</label>
                <input type="text"name="txt_nombre"class="form-control"placeholder="Nombre...">  
                </div>
                <div class="form-group">
                <label>Apellidos</label>
                <input type="text"name="txt_apellidos"class="form-control"placeholder="Apellidos...">  
                </div>


                <div class="form-group">
                <label>Correo electronico</label>
                <input type="text"name="txt_correo"class="form-control"placeholder="Correo Electronico...">  
                </div>

                <div class="form-group">
                <label>Contraseña</label>
                <input type="password"name="txt_contrasenha"class="form-control"placeholder="Contraseña...">  
              </div>               
              <input class="btn btn-secondary btn-block" type="submit" value="Guardar">  
        </form>
      </div>

      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

</body>

</html>